INSERT INTO projet (name, description)
VALUES ('projet1','creation kanban')
ON CONFLICT DO NOTHING;
INSERT INTO projet (name, description)
VALUES ('projet2','creation site gestion de taches')
ON CONFLICT DO NOTHING;
INSERT INTO projet (name, description)
VALUES ('projet3','Project management software')
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content,statut, projet_id)
VALUES ('Add a boutton', 'Ajouter un bouton rouge','TODO', 1);

INSERT INTO ticket (title, content, statut, projet_id)
VALUES ('Add à method', 'Ajouter une méthode qui affiche les articles', 'TODO', 1);

INSERT INTO ticket (title, content, statut, projet_id)
VALUES ('Add a template img', 'Ajouter un template', 'DONE', 2);

INSERT INTO ticket (title, content, statut, projet_id)
VALUES ('Add a template img', 'Ajouter un template', 'DONE', 2);

INSERT INTO ticket (title, content, statut, projet_id)
VALUES ('Add à method', 'Ajouter une méthode qui affiche les articles', 'TODO', 3);

