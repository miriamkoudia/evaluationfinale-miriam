DROP TABLE if EXISTS "ticket";
DROP TABLE if EXISTS "projet";

CREATE TABLE "projet" (
    "id" BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    "name" TEXT NOT NULL,
    "description"  VARCHAR(500),
     "creation_date" TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE "ticket" (
    "id" BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    "title"  VARCHAR(50),
    "content" VARCHAR(1000),
    "creation_date" TIMESTAMP NOT NULL DEFAULT now(),
    "statut" TEXT NOT NULL DEFAULT 'TO DO',
    "projet_id" INTEGER REFERENCES projet(id) ON DELETE CASCADE
);

