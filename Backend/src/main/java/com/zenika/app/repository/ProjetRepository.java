package com.zenika.app.repository;

import com.zenika.app.model.Projet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProjetRepository extends JpaRepository<Projet,Long> {

    // List<Projet> findAll();
    // Projet findById(long id);



}



