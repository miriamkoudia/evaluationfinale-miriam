package com.zenika.app.repository;

import com.zenika.app.model.Projet;
import com.zenika.app.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TicketRepository extends JpaRepository<Ticket,Long> {


    List<Ticket> findAllByOrderByIdDesc();


    }




