package com.zenika.app.service;

import com.zenika.app.exceptions.ProjetException;
import com.zenika.app.exceptions.TicketException;
import com.zenika.app.model.Projet;
import com.zenika.app.model.Ticket;
import com.zenika.app.repository.ProjetRepository;
import com.zenika.app.repository.TicketRepository;
import org.springframework.stereotype.Service;



import java.util.List;
import java.util.Optional;

@Service
public class ProjetService {

    private final ProjetRepository projetRepository;
    private final TicketRepository ticketRepository;

    public ProjetService(ProjetRepository projetRepository, TicketRepository ticketRepository) {
        this.projetRepository = projetRepository;
        this.ticketRepository = ticketRepository;
    }


    public Projet findById(Long id) throws ProjetException {
        Optional<Projet> projet = projetRepository.findById(id);
        if (projet.isPresent()) {
            return projet.get();
        } else {
            throw new ProjetException("Le projet n'existe pas !");
        }
    }


    public Projet update(Projet projet) {
        return projetRepository.save(projet);
    }

  /*  public void deleteById(long id) {
        projetRepository.deleteById(id);
    }
*/
    public List<Projet> findAll() {
        return projetRepository.findAll();
    }

    public void deleteProjet(Long id) throws Throwable {
        Projet projet = findById(id);
        projetRepository.delete(projet);

    }
}




























