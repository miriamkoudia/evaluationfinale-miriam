package com.zenika.app.service;

import com.zenika.app.exceptions.TicketException;
import com.zenika.app.model.Ticket;
import com.zenika.app.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketService {
    private final TicketRepository ticketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }


    public Ticket save(Ticket ticket) {
        return ticketRepository.save(ticket);

    }
    public List<Ticket> findAllTicket() {
        return ticketRepository.findAllByOrderByIdDesc();
    }

    public void deleteTicket(Long id) throws Throwable {
        Optional<Ticket> opsTicket = ticketRepository.findById(id);

        if (opsTicket.isPresent()) {
           Ticket ticketToDelete = opsTicket.get();
            ticketRepository.delete(ticketToDelete);
        } else {
            throw new TicketException("Le ticket n'existe pas !");
        }
    }

    public void updateTicket(Ticket ticket) {

        ticketRepository.save(ticket);
    }
}







