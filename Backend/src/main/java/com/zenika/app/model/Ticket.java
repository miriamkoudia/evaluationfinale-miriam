package com.zenika.app.model;

import com.zenika.app.enumerator.Statut;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;

import java.util.Date;

import static jakarta.persistence.GenerationType.IDENTITY;

@Data
@EqualsAndHashCode(of = "id")
@DynamicInsert
@Entity
@Table(name = "ticket")

public class Ticket {
    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private long id;
    @Column(name = "title")
    @Size(min = 1, max = 500, message = "Title must be between 1 and 50 characters")
    private String title;
    @Column(name = "content")
    @Size(min = 1, max = 1000, message = "Content must be between 1 and 1000 characters")
    private String content;
    @Column(name = "creation_date")
    private Date creationDate;
    @Column(name = "statut")
    @Enumerated(EnumType.STRING)
    private Statut statut;


}
