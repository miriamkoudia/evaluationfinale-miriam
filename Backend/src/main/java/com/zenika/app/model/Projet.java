package com.zenika.app.model;

import jakarta.persistence.*;

import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;

import java.util.Date;
import java.util.List;



@Data
@EqualsAndHashCode(of="id")
@DynamicInsert
@Entity
@Table(name="projet")
public class Projet {
    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    public String name;
    @Column(name = "description")
    @Size(min = 1, max = 500, message
            = "Description must be between 1 and 500 characters")
    public String description;
    @Column(name = "creation_date")
    public Date creationDate;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="projet_id")
    private List<Ticket> tickets;
}
