package com.zenika.app.controller;

import com.zenika.app.model.Ticket;
import com.zenika.app.repository.TicketRepository;
import com.zenika.app.service.TicketService;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tickets")
public class ticketController {
    @Autowired
    private final TicketService ticketService;
    @Autowired
    private final TicketRepository ticketRepository;

    public ticketController(TicketService ticketService, TicketRepository ticketRepository) {
        this.ticketService = ticketService;
        this.ticketRepository = ticketRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Ticket save(@RequestBody @Valid Ticket ticket) {
        return ticketService.save(ticket);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Ticket>> getAll() {
        List<Ticket> tickets = ticketService.findAllTicket();
        return new ResponseEntity<>(tickets, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTicket(@PathVariable Long id) throws Throwable {
        try {
            ticketService.deleteTicket(id);
            return ResponseEntity.ok("Le ticket a bien été supprimé");
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateTicket(@PathVariable Long id, @RequestBody Ticket ticket) {
        try {
            ticketService.updateTicket(ticket);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("ok!");
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<Ticket>> getQuestion(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(ticketRepository.findById(id));
    }

}
















/*public class QuestionController {

    public QuestionService questionService;

    public QuestionRepository questionRepository;

    public QuestionController(QuestionService questionService, QuestionRepository questionRepository ) {
        this.questionService = questionService;
        this.questionRepository = questionRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> addQuestion(@RequestBody Question question) {
        try {
            questionService.saveQuestion(question);
            return ResponseEntity.status(HttpStatus.OK).body("ok!");
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/{id}" ,method = RequestMethod.PUT)
    public ResponseEntity<String> updateQuestion(@PathVariable Long id, @RequestBody Question question) {
        try {
            questionService.updateQuestion(id,question);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("ok!");
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<Optional<Question>> getQuestion(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(questionRepository.findById(id));
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteQuestion(@PathVariable Long id) throws Throwable {
        try {
            questionService.deleteQuestion(id);
            return ResponseEntity.ok("La question a bien été supprimé");
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
       @PostMapping
    public Ticket save(@RequestBody @Valid Ticket ticket) {
        return ticketService.save(ticket);
    }

    @GetMapping
    public ResponseEntity<List<Ticket>> getAll() {
        List<Ticket> tickets = ticketService.findAllTicket();
        return new ResponseEntity<>(tickets, HttpStatus.OK);
    }

  @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> addTicket(@RequestBody Ticket ticket) {
        try {
            ticketService.saveTicket(ticket);
            return ResponseEntity.status(HttpStatus.OK).body("ok!");
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/{id}" ,method = RequestMethod.PUT)
    public ResponseEntity<String> updateTicket(@PathVariable Long id, @RequestBody Ticket ticket) {
        try {
            ticketService.updateTicket(id,ticket);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("ok!");
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
  @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<Optional<Ticket>> getTicket(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(ticketRepository.findById(id));
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTicket(@PathVariable Long id) throws Throwable {
        try {
            ticketService.deleteTicket(id);
            return ResponseEntity.ok("Le ticket a bien été supprimé");
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
*/