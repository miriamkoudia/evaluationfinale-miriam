package com.zenika.app.controller;

import com.zenika.app.exceptions.ProjetException;
import com.zenika.app.exceptions.TicketException;
import com.zenika.app.model.Projet;
import com.zenika.app.repository.ProjetRepository;
import com.zenika.app.service.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/projets")
@RestController
public class ProjetController {

    @Autowired
    private ProjetService projetService;
    @Autowired
    private ProjetRepository projetRepository;


    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Projet>> getAll(){
        return new ResponseEntity<>(projetService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Projet> getById(@PathVariable("id") Long id) throws ProjetException {
        return new ResponseEntity<>(projetService.findById(id), HttpStatus.ACCEPTED);
    }



    @RequestMapping(value = {""}, method = RequestMethod.PUT)
    public ResponseEntity<Projet> update(@RequestBody Projet projet){
        return new ResponseEntity<>(projetService.update(projet), HttpStatus.PARTIAL_CONTENT);
    }


   /* @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProjet(@PathVariable long id) {
        projetRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Projet supprimé avec succès.");
    }*/
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteProjet(@PathVariable Long id) throws Throwable {
        try {
            projetService.deleteProjet(id);
            return ResponseEntity.ok("Le projet a bien été supprimé");
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

}





