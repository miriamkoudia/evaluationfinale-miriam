#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER app_user ENCRYPTED PASSWORD 'eval_pass';
    CREATE DATABASE postgres OWNER app_user;
    GRANT ALL PRIVILEGES ON DATABASE postgres TO eval_user;
    GRANT ALL ON schema public TO app_user;
EOSQL
