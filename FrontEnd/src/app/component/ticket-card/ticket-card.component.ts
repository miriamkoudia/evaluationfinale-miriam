import { Component,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Projet } from 'src/app/model/projet';
import { Ticket } from 'src/app/model/ticket';
import { ProjetService } from 'src/app/services/projet.service';
import { TicketCardService } from 'src/app/services/ticket-card.service';



@Component({
  selector: 'app-ticket-card',
  templateUrl: './ticket-card.component.html',
  styleUrls: ['./ticket-card.component.css']
})
export class TicketCardComponent {
  id: number=0
  ticket!: Ticket 

  constructor(private ticketService: TicketCardService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id =Number(this.route.snapshot.paramMap.get('id'));
    console.log(this.id)
    this.ticketService.getTicketDetail(this.id).subscribe(ticket => {
      this.ticket = ticket
      console.log(this.ticket)
    })

  }

  updateTicketPage(ticket: Ticket) {
    console.log("modifions un ticket")
    this.router.navigateByUrl(`/ticket/${ticket.id}/update`)
  }


}
