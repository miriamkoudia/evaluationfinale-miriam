
import { Projet } from '../../model/projet';
import {Component,
  Input,
  OnInit,
} from '@angular/core'
import { ProjetService } from '../../services/projet.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  projets:Projet[]=[];
  projetDetail:Projet |undefined;
   // Add project properties
   newProject: Projet = {
    id: 0,
    name: '',
    description: '',
    creation_date: new Date(),
    tickets : []
  };

constructor(private projectService:ProjetService){}

ngOnInit(): void {
  this.projectService.getProjets().subscribe(response => {this.projets=response;})
};
 
selectProjetParent(projet:Projet){
  this.projectService.getProjetDetail(projet.id).subscribe(response =>{
    this.projetDetail=response
  })
}

addProject() {
  this.projectService.addProjet(this.newProject).subscribe(response => {
    this.projets.push(response);
    // Clear the new project object
    this.newProject = {
      id : 0,
      name: '',
      description: '',
      creation_date: new Date(),
      tickets : []
    };
  });
}

}
