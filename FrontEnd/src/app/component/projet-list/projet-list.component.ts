import { Component,Output,Input ,ChangeDetectionStrategy,
  ChangeDetectorRef,OnInit,EventEmitter } from '@angular/core';

import { Projet } from 'src/app/model/projet';

import { ProjetService } from 'src/app/services/projet.service';


@Component({
  selector: 'app-projet-list',
  templateUrl: './projet-list.component.html',
  styleUrls: ['./projet-list.component.css']
})
export class ProjetListComponent {
  @Input()
  projets:Projet[]=[]
  showForm = false;
  newProject: Projet = {
    name: '',
    description: '',
    id: 0,
    creation_date: new Date(),
    tickets : []
  };

 

  constructor(public projetService:ProjetService){}

  addProject(): void {
    this.projetService.addProjet(this.newProject).subscribe((project) => {
      this.projets.push(project);
      this.newProject = {
        name: '',
        description: '',
        id: 0,
        creation_date: new Date(),
        tickets : []
      };
      this.showForm = false;
    });
  }
 
  }



