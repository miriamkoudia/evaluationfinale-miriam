import { Component } from '@angular/core';
import { Projet } from '../../model/projet';
import { Ticket } from '../../model/ticket';
import { ProjetService } from '../../services/projet.service';
import { TicketCardService } from '../../services/ticket-card.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-projet-detail',
  templateUrl: './projet-detail.component.html',
  styleUrls: ['./projet-detail.component.css']
})
export class ProjetDetailComponent {
  projetDetail: Projet | undefined;
  tickets: Ticket[] = [];
  showAddTicketModal = false;
  newTicket: Ticket = {
    title: '',
    content: '',
    statut: 'Nouveau',
    creation_date: 0,
    projet_id: 2
  };

  constructor(
    public projetService: ProjetService,
    private route: ActivatedRoute,
    public ticketCardService: TicketCardService,
    private router: Router // Import the router service
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params): void => {
      const id = Number(params['id']);
      this.projetService.getProjetDetail(id).subscribe((projet) => {
        this.projetDetail = projet;
        this.tickets = projet.tickets;
      });
    });
  }

  openAddTicketModal() {
    this.showAddTicketModal = true;
  }

  deleteTicket(ticketId: number | undefined): void {
    if (ticketId) {
      const id = ticketId.toString(); // convert ticketId to a string
      this.ticketCardService.deleteTicket(parseInt(id)).subscribe(() => {
        this.tickets = this.tickets.filter((ticket) => ticket.id !== ticketId);
      });
    }
  }
  

  addTicket() {
    this.newTicket.projet_id = this.projetDetail?.id || 0;
    this.ticketCardService
      .addTicket(this.newTicket)
      .subscribe((ticket) => {
        this.tickets.push(ticket);
        this.newTicket = {
          title: '',
          content: '',
          statut: 'Nouveau',
          creation_date: 0,
          projet_id: 2
        };
        this.closeAddTicketModal();
      });
  }

  closeAddTicketModal() {
    this.showAddTicketModal = false;
  }

  deleteProjet() {
    if (this.projetDetail) {
      const projetId = this.projetDetail.id;
      this.projetService.deleteProjet(projetId).subscribe(() => {
        this.router.navigateByUrl('/'); // Navigate back to the projects list
      });
    }
  }

  generateTime(): string {
    return "28-04-23 00:00:00";
  }
}
