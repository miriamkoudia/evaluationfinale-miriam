import { Injectable } from '@angular/core';
import { Ticket } from '../model/ticket';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TicketCardService {
  private API_URL = '/api';
  tickets: Ticket[] = [];

  constructor(private http: HttpClient) {}

  getTickets(): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.API_URL}/tickets`);
  }

  getTicketDetail(id: number): Observable<Ticket> {
    return this.http.get<Ticket>(`${this.API_URL}/tickets/${id}`);
  }

  addTicket(payload: Omit<Ticket, 'id'>): Observable<Ticket> {
    return this.http.post<Ticket>(`${this.API_URL}/tickets/9`, payload).pipe(
      catchError((error: any) => this.handleError(error))
    );
  }

  deleteTicket(ticketId: number): Observable<Ticket> {
    return this.http
      .delete<Ticket>(`${this.API_URL}/tickets/${ticketId}`)
      .pipe(catchError((error: any) => this.handleError(error)));
  }


  updateTicket(ticketId: string | number, payload: Ticket): Observable<Ticket> {
    return this.http.patch<Ticket>(`${this.API_URL}/tickets/${ticketId}`, payload).pipe(
      catchError((error: any) => this.handleError(error))
    );
  }

  private handleError(error: any) {
    let message: string;

    if (error.error instanceof ErrorEvent) {
      message = `${error.error.message}`;
    } else {
      message = `${error.message}`;
    }

    return throwError(() => new Error(message));
  }
}

