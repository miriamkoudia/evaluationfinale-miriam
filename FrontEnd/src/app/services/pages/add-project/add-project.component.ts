import { Component } from '@angular/core';
import { ProjetService } from '../../projet.service';
import { Projet } from '../../../model/projet';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent {


 constructor(private projetService: ProjetService, private router: Router) { }
  handleCreateProjet(projet:Projet): void{
    this.projetService.addProjet(projet).subscribe(projet =>this.router.navigateByUrl(`/`))
  }

}
















// projects: Projet[] = []
  // newProject: Projet = {
  //   id: undefined,
  //   name: '',
  //   description: '',
  //   creation_date: undefined,
  // }
// onSubmit() {
  
//   this.projetService.addProjet(this.newProject).subscribe(projet => {
//     this.newProject = { id: undefined, name: '', description: '', creation_date:undefined }
//   });
// }


// }



