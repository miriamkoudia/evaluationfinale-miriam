import { Component,OnInit } from '@angular/core';

import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Ticket, TicketDto } from 'src/app/model/ticket';
import { TicketCardService } from '../../ticket-card.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-update-ticket',
  templateUrl: './update-ticket.component.html',
  styleUrls: ['./update-ticket.component.css']
})
export class UpdateTicketComponent implements OnInit {

  id: string = '';
  ticket!: Ticket;

  myForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  })

  constructor(private ticketCardService: TicketCardService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
    this.id = snapshot.params['id'];
    this.ticketCardService.getTicketDetail(Number(this.id)).subscribe((ticket) => this.ticket = ticket);
  }

  submit():void {    
    if (this.myForm.valid) {
      const values = this.myForm.value as TicketDto;
      this.ticketCardService.updateTicket(this.id, {...this.ticket ,...values}).subscribe(() => {
        this.router.navigateByUrl(`/`)
      });
    }
  }
}

