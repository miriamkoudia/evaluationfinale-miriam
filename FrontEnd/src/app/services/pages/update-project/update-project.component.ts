import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot,Router } from '@angular/router';
import { Projet } from '../../../model/projet';
import { ProjetService } from '../../projet.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Ticket } from 'src/app/model/ticket';


@Component({
  selector: 'app-update-projet',
  templateUrl:'./update-project.component.html',
  styleUrls: ['./update-project.component.css']
})
export class UpdateProjetComponent implements OnInit {
  id: string = '';
  projet?: Projet;
constructor(private projetService: ProjetService, private route: ActivatedRoute, private router: Router) {}

ngOnInit(): void {
  const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
  this.id = snapshot.params['id'];
  this.projetService.getProjetByID(this.id).subscribe((projet) => this.projet = projet);
}
handleUpdateProjet(projet: Projet) {
  this.projetService.updateProjet (this.id,projet).subscribe(projet=> this.router.navigateByUrl(`/`))
}
myForm = new FormGroup({
  name: new FormControl('', [Validators.required]),
  description: new FormControl('', [Validators.required]),
})
update():void {    
  if (this.myForm.valid) {
    const values = this.myForm.value;
    const result = {...values} as unknown as Projet;
    this.projetService.updateProjet(this.id,result).subscribe(() => {
    
      const currentUrl = this.router.url;
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/']);
      });
    });
  }
}
}






































//   // updatedProject: Projet = {
//   //   id: undefined,
//   //   name: '',
//   //   description: '',
//   //   creation_date: undefined,
//   // };

//   constructor(private projetService: ProjetService, private route: ActivatedRoute) {}

//   ngOnInit(): void {
//     this.route.paramMap.subscribe(params => {
//       const projectID = params.get('id') ?? '';
//       this.projetService.getProjetByID(projectID).subscribe(projet => {
//         this.updatedProject = projet;
//       });    
//     });
//   }

//   onSubmit() {
//     if (this.updatedProject.id !== undefined) {
//       this.projetService.updateProjet(this.updatedProject.id, this.updatedProject).subscribe(projet => {
//         this.updatedProject = projet;
//       });
//     }
//   }
// }


