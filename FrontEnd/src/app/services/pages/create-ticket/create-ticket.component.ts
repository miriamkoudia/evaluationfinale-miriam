import { Component } from '@angular/core';
import { TicketCardService } from '../../ticket-card.service';
import { Ticket } from '../../../model/ticket';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.css']
})
export class CreateTicketComponent {

  constructor(private ticketCardService:TicketCardService, private router: Router ){}

  handlecreateTicket(ticket:Ticket):void{
    this.ticketCardService.addTicket(ticket).subscribe(ticket =>this.router.navigateByUrl(`/`));
  }
  myForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  })

  save():void {    

    if (this.myForm.valid) {
      console.log(this.myForm)
      const values = this.myForm.value 
      const result = {...values} as unknown as Ticket
      this.ticketCardService.addTicket(result).subscribe(() => {

        const actuelleUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate(['/']);
        });
      })
    }
  }
}


  


