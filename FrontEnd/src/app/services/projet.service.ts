import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Projet } from '../model/projet';
import { Ticket } from '../model/ticket';


@Injectable()
export class ProjetService {
  private API_URL = '/api';

  constructor(private http: HttpClient) {}

  getProjets(): Observable<Projet[]> {
    return this.http.get<Projet[]>(`${this.API_URL}/projets`);
  }

  getProjetDetail(id: number): Observable<Projet> {
    return this.http.get<Projet>(`${this.API_URL}/projets/${id}`);
  }

  addProjet(payload: Omit<Projet, 'id'>) {
    return this.http.put<Projet>(`${this.API_URL}/projets`, payload)
      .pipe(catchError((error: any) => this.handleError(error)));
  }

  deleteProjet(projetId: string | number) {
    return this.http.delete<Projet>(`${this.API_URL}/projets/${projetId}`)
      .pipe(catchError((error: any) => this.handleError(error)));
  }

  updateProjet(projetId: string | number, payload: Omit<Projet, 'id'>) {
    return this.http.patch<Projet>(`${this.API_URL}/projets/${projetId}`, payload)
      .pipe(catchError((error: any) => this.handleError(error)));
  }

  getProjetByID(id: string):Observable<Projet> {
    return this.http.get<Projet>(`${this.API_URL}/project/${id}`);
  }

  getTicketsForProjet(id: number): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.API_URL}/${id}/tickets`);
  }

  private handleError(error: any) {
    let message: string;

    if (error.error instanceof ErrorEvent) {
      message = `${error.error.message}`;
    } else {
      message = `${error.message}`;
    }

    return throwError(() => new Error(message));
  }
  
}