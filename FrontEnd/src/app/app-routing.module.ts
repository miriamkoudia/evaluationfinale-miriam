import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { ProjetDetailComponent } from './component/projet-detail/projet-detail.component';
import { AddProjectComponent } from './services/pages/add-project/add-project.component';
import { CreateTicketComponent } from './services/pages/create-ticket/create-ticket.component';
import { UpdateProjetComponent } from './services/pages/update-project/update-project.component';
import { UpdateTicketComponent } from './services/pages/update-ticket/update-ticket.component';

const routes: Routes = [
  {path:'', redirectTo:'home', pathMatch:'full'},
  {path:'home',component:HomeComponent},
  {path:'home/projets/:id', component:ProjetDetailComponent},
  {path: 'projets/add', component:AddProjectComponent },
  {path: 'ticket/create', component:CreateTicketComponent },
  {path: 'projets/:id/update', component:UpdateProjetComponent},
  {path: 'ticket/:id/update', component:UpdateTicketComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
