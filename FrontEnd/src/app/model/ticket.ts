export interface Ticket{
    id?:number 
    title:string
    content:string
    creation_date:number|undefined
    statut:string
    projet_id : number
}

export interface TicketDto{
    title:string
    content:string
    statut:string
}