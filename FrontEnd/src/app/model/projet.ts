import { Ticket } from './ticket';

export class Projet {
  constructor(
    public id: number ,
    public name: string,
    public description: string,
    public creation_date: Date ,
    public tickets: Ticket[]
  ) {}
}
