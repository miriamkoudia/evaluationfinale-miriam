import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './component/home/home.component';
import { HeaderComponent } from './component/header/header.component';
import { TicketCardComponent } from './component/ticket-card/ticket-card.component';
import { ProjetListComponent } from './component/projet-list/projet-list.component';
import { ProjetDetailComponent } from './component/projet-detail/projet-detail.component';
import { AddProjectComponent } from './services/pages/add-project/add-project.component';
import { CreateTicketComponent } from './services/pages/create-ticket/create-ticket.component';
import { UpdateTicketComponent } from './services/pages/update-ticket/update-ticket.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjetService } from './services/projet.service';
import { FormsModule } from '@angular/forms';
import { CommonModule, DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    TicketCardComponent,
    HeaderComponent,
    HomeComponent,
    ProjetListComponent,
    ProjetDetailComponent,
    CreateTicketComponent,
    UpdateTicketComponent,
    AddProjectComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ProjetService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
