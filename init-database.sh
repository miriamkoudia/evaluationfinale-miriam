#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER eval_user ENCRYPTED PASSWORD 'eval_pass';
    CREATE DATABASE evaluation OWNER eval_user;
    GRANT ALL PRIVILEGES ON DATABASE evaluation TO eval_user;
    GRANT ALL ON schema public TO eval_user;
EOSQL
